#include "stdafx.h"

#include <Psapi.h>
#include <vector>

#include "FltData.h"
#include "Log.h"
#include "DataCom.h"

#pragma comment(lib, "Psapi.lib")

//------------------------------------------
// ȫ�ֱ���
//------------------------------------------
std::vector<CString>		g_ProcArray;

PBOOL	g_lpDinput8Ctrl = NULL;
HANDLE	g_hMap = NULL;

//------------------------------------------
//------------------------------------------
void InitFltProc()
{
	AddOneProc(_T("CustomFormat.exe"));
	AddOneProc(_T("Client.exe"));
}

BOOL AddOneProc(LPCTSTR lpszProcName)
{
	if ( lpszProcName )
	{
		CString	strOneProc(lpszProcName);
		g_ProcArray.push_back(strOneProc);

		return TRUE;
	}
	
	return FALSE;
}

BOOL DelOneProc(LPCTSTR lpszProcName)
{
	if ( lpszProcName )
	{
		CString	strProcName(lpszProcName);
		std::vector<CString>::iterator iter = g_ProcArray.begin();

		for ( ; iter != g_ProcArray.end(); ++iter )
		{
			CString	strIter(*iter);
			if ( strProcName == strIter )
			{
				g_ProcArray.erase(iter);
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL FindOneProc(LPCTSTR lpszProcName)
{
	if ( lpszProcName )
	{
		CString	strProcName(lpszProcName);
		std::vector<CString>::iterator iter = g_ProcArray.begin();

		for ( ; iter != g_ProcArray.end(); ++iter )
		{
			CString	strIter(*iter);
			if ( strProcName == strIter )
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL IsHandleProcess()
{
	return TRUE;

	TCHAR szBuf[MAX_PATH] = { 0 };

	ULONG dwLength = GetModuleBaseName(GetCurrentProcess(), NULL, szBuf, MAX_PATH);
	if ( dwLength == 0 )
	{
		DbgViewMessage(_T("[IsFltProc] Failed! Error: %u\n"), GetLastError());
		SetLastError(GetLastError());

		return FALSE;
	}

	BOOL brt = FindOneProc(szBuf);
// 	brt ? 
// 		DbgViewMessage(_T("[IsHandleProcess] Filter This Proc: %ws\n"), szBuf) : 
// 		DbgViewMessage(_T("[IsHandleProcess] No Filter This Proc: %ws\n"), szBuf);

	return brt;
}

void InitMemShared()
{
	g_hMap = CreateFileMapping(
		INVALID_HANDLE_VALUE, 
		NULL, 
		PAGE_READWRITE | SEC_COMMIT, 
		0, 
		sizeof(BOOL), 
		STR_NAME_SHARE_MEM
		);
	if (g_hMap != NULL)
	{
		g_lpDinput8Ctrl = (PBOOL)MapViewOfFile(g_hMap, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
		if (g_hMap == NULL)
		{
			DbgViewMessage(_T("[InitMemShared] MapViewOfFile Failed! Err: %u\n"), GetLastError());
			CloseHandle(g_hMap);
			g_hMap = NULL;
			return;
		}

		DbgViewMessage(_T("[InitMemShared] Success!\n"));
	}

	DbgViewMessage(_T("[InitMemShared] CreateFileMapping Failed! Err: %u\n"), GetLastError());
}

void UnInitMemShared()
{
	if ( g_lpDinput8Ctrl )
	{
		DbgViewMessage(_T("[UnInitMemShared] g_lpDinput8Ctrl Start!\n"));
		UnmapViewOfFile(g_lpDinput8Ctrl);
		g_lpDinput8Ctrl = NULL;
	}

	if ( g_hMap )
	{
		DbgViewMessage(_T("[UnInitMemShared] g_hMap Start!\n"));
		CloseHandle(g_hMap);
		g_hMap = NULL;
	}
}

BOOL IsStartDinput8()
{
	BOOL brt = FALSE;

	if ( g_lpDinput8Ctrl )
	{
		CopyMemory(&brt, g_lpDinput8Ctrl, sizeof(BOOL));
	}

	return brt;
}