// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件:
#include <windows.h>


// TODO: 在此处引用程序需要的其他头文件
#include <Unknwn.h>
#include <objbase.h>

#include <tchar.h>
#include <strsafe.h>

#include <atlstr.h>

#define IS_NULL(_p)		( (_p) == NULL )

// 包含dinput.h时，需要.
// 必须先定义，否则报错：DIRECTINPUT_VERSION undefined. Defaulting to version 0x0800
#define DIRECTINPUT_VERSION 0x0800