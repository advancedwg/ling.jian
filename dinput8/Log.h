#pragma once

#include <dinput.h>

void DbgViewMessage(TCHAR *Format, ...);

void DbgViewGuid(TCHAR *strPre, const GUID *guid);

void DbgViewDeviceObjectData(LPDIDEVICEOBJECTDATA lpDevObjData);