#include "stdafx.h"
#include <stdio.h>
#include <stdarg.h>

#include "Log.h"

void DbgViewMessage(TCHAR *Format, ...)
{
	va_list	argptr;
	va_start(argptr, Format);

	TCHAR	szBuf[MAX_PATH] = { 0 };

	_vsntprintf_s(szBuf, MAX_PATH ,Format, argptr);

	va_end(argptr);

	OutputDebugString(szBuf);
}

void DbgViewGuid(TCHAR *strPre, const GUID *guid)
{
	DbgViewMessage(_T("%s%08X-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X\n"), 
		strPre, 
		guid->Data1, guid->Data2, guid->Data3, 
		guid->Data4[0], guid->Data4[1], guid->Data4[2], guid->Data4[3], 
		guid->Data4[4], guid->Data4[5], guid->Data4[6], guid->Data4[7]);
}

void DbgViewDeviceObjectData(LPDIDEVICEOBJECTDATA lpDevObjData)
{
	DbgViewMessage(_T("���к�: %u\n"), lpDevObjData[0].dwSequence);
	switch ( lpDevObjData[0].dwOfs )
	{
	case DIMOFS_X:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_X\n"));
		break;
	case DIMOFS_Y:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_Y\n"));
		break;
	case DIMOFS_Z:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_Z\n"));
		break;
	case DIMOFS_BUTTON0:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_BUTTON0\n"));
		break;
	case DIMOFS_BUTTON1:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_BUTTON1\n"));
		break;
	case DIMOFS_BUTTON2:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_BUTTON2\n"));
		break;
	case DIMOFS_BUTTON3:
		DbgViewMessage(_T("\t\t\t\tDIMOFS_BUTTON3\n"));
		break;
	default:
		DbgViewMessage(_T("\t\t\t\tdwOfs: %u\n"), lpDevObjData[0].dwOfs);
		break;
	}
	DbgViewMessage(_T("\t\t\t\tdwData: %#x(%u)\n"), 
		lpDevObjData[0].dwData, lpDevObjData[0].dwData);
	DbgViewMessage(_T("\t\t\t\tdwTimeStamp: %#x(%u)\n"), 
		lpDevObjData[0].dwTimeStamp, lpDevObjData[0].dwTimeStamp);
	DbgViewMessage(_T("\t\t\t\tuAppData: %#x(%u)\n"), 
		lpDevObjData[0].uAppData, lpDevObjData[0].uAppData);
}