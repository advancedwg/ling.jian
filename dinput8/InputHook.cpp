#include "stdafx.h"
#include <dinput.h>

#include "InputHook.h"
#include "Log.h"
#include "FltData.h"
#include "DataCom.h"

//------------------------------------------
// 1. pInterface->CreateDevice
// 2. pSysMouse->Acquire;
// 3. pKeyboard->Acquire;
// 4. pSysMouse->Poll;
// 5. pKeyboard->Poll;
// 6. pSysMouse->GetDeviceState;
// 7. pSysMouse->GetDeviceState;
//------------------------------------------
PROC		g_pfnOldCreateDevice = NULL;

PROC		g_pfnAcquire = NULL;

PROC		g_pfnPoll = NULL;

PROC		g_pfnGetDeviceState = NULL;

PROC		g_pfnSetCooperativeLevel = NULL;

PROC		g_pfnSetDataFormat = NULL;

PROC		g_pfnGetDeviceData = NULL;

//------------------------------------------
// 1. Point to the mouse device;
//	use it to hook Acquire, Poll and GetDeviceState
// 2. Point to the keyboard device;
//------------------------------------------

LPDIRECTINPUTDEVICE8	g_lpMouseDevice = NULL;
LPDIRECTINPUTDEVICE8	g_lpKeyboardDevice = NULL;

//------------------------------------------
// 
//------------------------------------------
ULONG	g_dwCount = 0;

HRESULT STDMETHODCALLTYPE
MyGetDeviceData(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	DWORD cbObjectData,
	LPDIDEVICEOBJECTDATA rgdod,
	LPDWORD pdwInOut,
	DWORD dwFlags
	)
{
	HRESULT	hr = S_FALSE;

	pfnGetDeviceData	pfnFun = NULL;

	static BOOL bF = TRUE;
	static ULONG dwSeque = 0;
	static ULONG dwNumSend = 0;

	if ( pdwInOut && rgdod )
	{
		if ( ((dwNumSend ++) % 10) == 0 && lpDevice == g_lpMouseDevice )
		{
			if ( GetOneMouse(rgdod) )
			{
				rgdod[0].dwSequence = dwSeque + 1;
				rgdod[0].dwTimeStamp = GetTickCount();

				*pdwInOut = 1;

				return DI_OK;
			}
		}
		// 			if ( IsHandleProcess() && 
		// 				IsStartDinput8() && 
		// 				*pdwInOut == 1 && 
		// 				/*((dwNumSend++ % 50) == 0) && */dwNumSend == 0
		// 				)
		// 			{
		// // 				rgdod[0].dwOfs = DIMOFS_X;
		// // 				rgdod[0].dwData = 45;
		// 				rgdod[0].dwOfs = DIMOFS_BUTTON0;
		// 				rgdod[0].dwData = /*bF ? 0x80 : 0*/0x80;
		// 
		// 				rgdod[0].dwSequence = dwSeque + 1;
		// 				rgdod[0].dwTimeStamp = bF ? GetTickCount() : (GetTickCount() + 150);
		// 				rgdod[0].uAppData = 0xFFFFFFFF;
		// 
		// 				bF = bF ? FALSE : TRUE;
		// 
		// 				dwSeque = rgdod[0].dwSequence;
		// 
		// 				DbgViewMessage(_T("============构造鼠标事件============\n"));
		// 				DbgViewDeviceObjectData(rgdod);
		// 
		// 				*pdwInOut = 1;
		// 				dwNumSend = 1;
		// 
		// 				return DI_OK;
		// 			}

	}

	pfnFun = (pfnGetDeviceData)((PVOID)g_pfnGetDeviceData);
	if ( pfnFun )
	{
		hr = pfnFun(lpDevice, cbObjectData, rgdod, pdwInOut, dwFlags);
		if ( SUCCEEDED(hr) )
		{
			if ( pdwInOut )
			{
				if ( *pdwInOut > 0 && rgdod )
				{
					dwSeque = rgdod[(*pdwInOut) - 1].dwSequence;
// 					for ( ULONG i = 0; i < *pdwInOut; ++i )
// 					{
// 						if ( rgdod[i].dwOfs != DIMOFS_X && rgdod[i].dwOfs != DIMOFS_Y )
// 						{
// 							DbgViewMessage(_T("[%02d]============鼠标事件============\n"), *pdwInOut);
// 							if ( rgdod[i].dwOfs == DIMOFS_BUTTON1 && IsStartDinput8() )
// 							{
// 								DbgViewMessage(_T("修改：右键为左键\n"));
// 								rgdod[i].dwOfs = DIMOFS_BUTTON0;
// 							}
// 							DbgViewDeviceObjectData(rgdod);
// 						}
// 					}
				}
			}
		}
	}

	return hr;
}

HRESULT STDMETHODCALLTYPE
MySetDataFormat(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	LPCDIDATAFORMAT lpdf
	)
{
	HRESULT	hr = S_FALSE;

	pfnSetDataFormat	pfnFun = NULL;

	pfnFun = (pfnSetDataFormat)((PVOID)g_pfnSetDataFormat);
	if ( pfnFun )
	{
		hr = pfnFun(lpDevice, lpdf);
		if ( SUCCEEDED(hr) )
		{
			LPDIOBJECTDATAFORMAT rgodf = lpdf->rgodf;
			if ( rgodf )
			{
				DbgViewMessage(_T("\t\t&c_dfDIMouse: %#08x, dwFlags: %08u, OnePacketSize: %08u, NumOfObj: %08u\n"), 
					&c_dfDIMouse, c_dfDIMouse.dwFlags, c_dfDIMouse.dwDataSize, c_dfDIMouse.dwNumObjs);
				for ( ULONG i = 0; i < c_dfDIMouse.dwNumObjs; ++i )
				{
					DbgViewMessage(_T("\t\tDIOBJECTDATAFORMAT: %02u\n"), i);
					if ( c_dfDIMouse.rgodf[i].pguid )
					{
						DbgViewGuid(_T("\t\t\t\t\t\tGUID: "), c_dfDIMouse.rgodf[i].pguid);
					}
					DbgViewMessage(_T("\t\t\t\t\t\tdwOfs: %08u, dwType: %#08x, dwFlags: %08u\n"), 
						c_dfDIMouse.rgodf[i].dwOfs, c_dfDIMouse.rgodf[i].dwType, c_dfDIMouse.rgodf[i].dwFlags);
				}

				DbgViewGuid(_T("\t\tGUID_XAxis: "), &GUID_XAxis);
				DbgViewGuid(_T("\t\tGUID_YAxis: "), &GUID_YAxis);
				DbgViewGuid(_T("\t\tGUID_ZAxis: "), &GUID_ZAxis);

				DbgViewMessage(_T("==========================================\n"));
				DbgViewMessage(_T("\t\tlpdf: %#08x, dwFlags: %08u, OnePacketSize: %08u, NumOfObj: %08u, &c_dfDIMouse: %#08x, &c_dfDIMouse2: %#08x\n"), 
					lpdf, lpdf->dwFlags, lpdf->dwDataSize, lpdf->dwNumObjs, &c_dfDIMouse, &c_dfDIMouse2);

				for ( ULONG i = 0; i < lpdf->dwNumObjs; ++i )
				{
					TCHAR	tchar = ' ';
					DbgViewMessage(_T("\t\tDIOBJECTDATAFORMAT: %02u\n"), i);

					if ( rgodf[i].pguid )
					{
						DbgViewGuid(_T("\t\t\t\t\t\tGUID: "), rgodf[i].pguid);
						if ( memcmp(rgodf[i].pguid, c_dfDIMouse.rgodf[0].pguid, sizeof(GUID)) == 0 )
						{
							tchar = _T('X');
						}
						else if ( memcmp(rgodf[i].pguid, c_dfDIMouse.rgodf[1].pguid, sizeof(GUID)) == 0 )
						{
							tchar = _T('Y');
						}
						else if ( memcmp(rgodf[i].pguid, c_dfDIMouse.rgodf[2].pguid, sizeof(GUID)) == 0 )
						{
							tchar = _T('Z');
						}
					}

					DbgViewMessage(_T("\t\t\t\t\t\t%c (dwOfs: %08u, dwType: %#08x, dwFlags: %08u)\n"), 
						tchar, rgodf[i].dwOfs, rgodf[i].dwType, rgodf[i].dwFlags);
				}
			}
		}
	}

	return hr;
}

HRESULT STDMETHODCALLTYPE 
MySetCooperativeLevel(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	HWND hwnd, 
	DWORD dwFlags
	)
{
	HRESULT	hr = S_FALSE;

	pfnSetCooperativeLevel	pfnFun = NULL;

	pfnFun = (pfnSetCooperativeLevel)((PVOID)g_pfnSetCooperativeLevel);
	if ( pfnFun )
	{
		if ( dwFlags & DISCL_FOREGROUND )
		{
			dwFlags &= ~DISCL_FOREGROUND;
			dwFlags |= DISCL_BACKGROUND;
			DbgViewMessage(_T("[MySetCooperativeLevel] dwFlags: %u\n"), dwFlags);
		}

		hr = pfnFun(lpDevice, hwnd, dwFlags);
	}

	return hr;
}

HRESULT STDMETHODCALLTYPE 
MyGetDeviceState(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	ULONG	dwBufSize, 
	LPVOID	lpBuf
	)
{
	HRESULT hr = S_FALSE;
	pfnGetDeviceState	pfnFun = NULL;

	if ( lpDevice == g_lpKeyboardDevice )
	{
		if ( GetOneKeyboard((char *)lpBuf, dwBufSize) )
		{
			return DI_OK;
		}
	}

	pfnFun = (pfnGetDeviceState)((PVOID)g_pfnGetDeviceState);
	// 调用原始的函数，不存在时，直接PASS
	if ( pfnFun )
	{
		hr = pfnFun(lpDevice, dwBufSize, lpBuf);
 		if ( SUCCEEDED(hr) )
 		{
			DbgViewMessage(_T("[MyGetDeviceState] Success!\n"));
 		}
	}

	return hr;
}

// HRESULT STDMETHODCALLTYPE 
// MyPoll(
// 	LPDIRECTINPUTDEVICE8 lpDevice
// 	)
// {
// 	HRESULT hr = S_FALSE;
// 	pfnPoll	pfnFun = NULL;
// 
// 	// 鼠标设备
// 	if ( lpDevice == g_lpMouseDevice )
// 	{
// 		DbgViewMessage(_T("[MyPoll] Mouse Poll!\n"));
// 		pfnFun = (pfnPoll)((PVOID)g_pfnMouseAcquire);
// 	}
// 
// 	// 键盘设备
// 	else if ( lpDevice == g_lpKeyboardDevice )
// 	{
// 		DbgViewMessage(_T("[MyPoll] Keyboard Poll!\n"));
// 		pfnFun = (pfnPoll)((PVOID)g_pfnKeyboardAcquir);
// 	}
// 
// 	// 调用原始的函数，不存在时，直接PASS
// 	if ( pfnFun )
// 	{
// 		hr = pfnFun(lpDevice);
// 
// 		// 如果是我们的设备，返回OK。
// 		if ( IsHandleProcess() )
// 		{
// 			return S_OK;
// 		}
// 	}
// 	return hr;
// }

/*------------------------------------------
** 函数：
**	MyAcquire
** 参数：
**	lpDevice，为原始函数的this指针
** 说明：
**	当界面不在当前的时候，会调用该函数。
------------------------------------------*/
// HRESULT STDMETHODCALLTYPE 
// MyAcquire(
// 	LPDIRECTINPUTDEVICE8 lpDevice
// 	)
// {
// 	HRESULT hr = S_FALSE;
// 	pfnAcquire	pfnFun = NULL;
// 
// 	// 鼠标设备
// 	if ( lpDevice == g_lpMouseDevice )
// 	{
// 		DbgViewMessage(_T("[MyAcquire] Mouse Acquire!\n"));
// 		pfnFun = (pfnAcquire)((PVOID)g_pfnMouseAcquire);
// 	}
// 
// 	// 键盘设备
// 	else if ( lpDevice == g_lpKeyboardDevice )
// 	{
// 		DbgViewMessage(_T("[MyAcquire] Keyboard Acquire!\n"));
// 		pfnFun = (pfnAcquire)((PVOID)g_pfnKeyboardAcquir);
// 	}
// 
// 	// 调用原始的函数，不存在时，直接PASS
// 	if ( pfnFun )
// 	{
// 		// 如果是我们的设备，返回OK。
// 		if ( IsHandleProcess() )
// 		{
// //			return S_OK;
// 		}
// 
// 		hr = pfnFun(lpDevice);
// 		DbgViewMessage(_T("[MyAcquire] Call Old Acquire!\n"));
// 	}
// 
// 	return hr;
// }

/*------------------------------------------
** 函数：
**	MyCreateDevice
** 参数：
**	lpDeviceInterface，为原始函数的this指针
**	其他参数同原始函数。
** 说明：
**	创建鼠标/键盘/游戏操纵杆等支持设备的时候，调用该函数
------------------------------------------*/
HRESULT STDMETHODCALLTYPE 
MyCreateDevice(
	LPDIRECTINPUT8 lpDeviceInterface, 
	REFGUID Guid, 
	LPDIRECTINPUTDEVICE8 *ppDirectinputDevice8, 
	LPUNKNOWN lpUnknown
	)
{
	// 调用原始创建设备的函数
	HRESULT	hResult = ((pfnCreateDevice)g_pfnOldCreateDevice)(lpDeviceInterface, Guid, ppDirectinputDevice8, lpUnknown);

	if ( SUCCEEDED(hResult) )
	{
		// 保存鼠标创建好的设备
		if ( GUID_SysMouse == Guid )
		{
			g_lpMouseDevice = *ppDirectinputDevice8;
			DbgViewMessage(_T("[MyCreateDevice] Mouse Success! g_lpMouseDevice: %#x\n"), g_lpMouseDevice);

			// Hook鼠标设备下的Acquire函数
			HookInterface(g_lpMouseDevice);
		}

		// 保存键盘创建好的设备
		else if ( GUID_SysKeyboard == Guid )
		{
			g_lpKeyboardDevice = *ppDirectinputDevice8;
			DbgViewMessage(_T("[MyCreateDevice] Keyboard Success! g_lpKeyboardDevice: %#x\n"), g_lpKeyboardDevice);

			// Hook键盘设备下的Acquire、Poll、GetDeviceState函数
//			HookInterface(g_lpKeyboardDevice, TYPE_DEV_KEYBOARD);
		}
	}
	
	return hResult;
}

BOOL HookCreateDevice(LPVOID pInterface)
{
	BOOL	brt = FALSE;

	if ( pInterface )
	{
		brt = ModifyVirtualFunTable(pInterface, HOOK_INDEX_CreateDevice, (const PROC)MyCreateDevice, &g_pfnOldCreateDevice);
	}

	if ( brt )
	{
		DbgViewMessage(_T("[HookCreateDevice] Success!\n"));
	}

	return brt;
}

BOOL HookInterface(LPVOID pDevice)
{
	BOOL brt = HookInterfaceByDevice(pDevice, HOOK_INDEX_SetDataFormat, (const PROC)MySetDataFormat);
	brt = HookInterfaceByDevice(pDevice, HOOK_INDEX_SetCooperativeLevel, (const PROC)MySetCooperativeLevel);
	brt = HookInterfaceByDevice(pDevice, HOOK_INDEX_GetDeviceState, (const PROC)MyGetDeviceState);
//	brt = HookInterfaceByDevice(pDevice, Type, HOOK_INDEX_GetDeviceData, (const PROC)MyGetDeviceData);

	return brt;
}

BOOL HookInterfaceByDevice(LPVOID pDevice, ULONG IndexFun, const PROC NewFun)
{
	BOOL	brt = FALSE;
	TCHAR	szBuf[MAX_PATH] = { 0 };
	PROC	pfnProc = NULL;

	if ( pDevice == NULL )
	{
		return FALSE;
	}

	brt = ModifyVirtualFunTable(pDevice, IndexFun, NewFun, &pfnProc);
	if ( !brt )
	{
		DbgViewMessage(_T("[HookInterfaceByDevice] ModifyVirtualFunTable Failed!\n"));
		return brt;
	}

	switch ( IndexFun )
	{
// 	case HOOK_INDEX_Acquire:
// 		{
// 			if ( Type == TYPE_DEV_MOUSE )
// 			{
// 				g_pfnMouseAcquire = *pfnProc;
// 				_tcscpy_s(szBuf, _T("Mouse Func: Acquire!"));
// 			} 
// 			else if ( Type == TYPE_DEV_KEYBOARD )
// 			{
// 				g_pfnKeyboardAcquir = *pfnProc;
// 				_tcscpy_s(szBuf, _T("Keyboard Func: Acquire"));
// 			}
// 		}
// 		break;
// 	case HOOK_INDEX_Poll:
// 		{
// 			if ( Type == TYPE_DEV_MOUSE )
// 			{
// 				g_pfnMousePoll = *pfnProc;
// 				_tcscpy_s(szBuf, _T("Mouse Func: Poll!"));
// 			} 
// 			else if ( Type == TYPE_DEV_KEYBOARD )
// 			{
// 				g_pfnKeyboardPoll = *pfnProc;
// 				_tcscpy_s(szBuf, _T("Keyboard Func: Poll"));
// 			}
// 		}
// 		break;
	case HOOK_INDEX_GetDeviceState:
		{
			g_pfnGetDeviceState = *pfnProc;
			_tcscpy_s(szBuf, _T("Func: GetDeviceState!"));
		}
		break;

	case HOOK_INDEX_SetCooperativeLevel:
		{
			g_pfnSetCooperativeLevel = *pfnProc;
			_tcscpy_s(szBuf, _T("Func: SetCooperativeLevel!"));
		}
		break;

	case HOOK_INDEX_SetDataFormat:
		{
			g_pfnSetDataFormat = *pfnProc;
			_tcscpy_s(szBuf, _T("Func: SetCooperativeLevel!"));
		}
		break;

	case HOOK_INDEX_GetDeviceData:
		{
			g_pfnGetDeviceData = *pfnProc;
			_tcscpy_s(szBuf, _T("Func: GetDeviceData!"));
		}
		break;
	default:
		break;
	}

	if ( brt )
	{
		DbgViewMessage(_T("[HookInterfaceByDevice] Success! Device: %ws\n"), szBuf);
	}

	return brt;
}

BOOL ModifyVirtualFunTable(LPVOID pInterface, int nFunIndex, const PROC pNewFun, PROC *pSaveOldFun)
{
	BOOL	bResult = FALSE;

	if ( IS_NULL(pInterface) || IS_NULL(pNewFun) || IS_NULL(pSaveOldFun) )
	{
		DbgViewMessage(_T("[ModifyVirtualFunTable] Parameter Error! Interface: %#x, nFunIndex: %#x, NewFun: %#x, SaveOldFun: %#x\n"), 
			pInterface, nFunIndex, pNewFun, pSaveOldFun);
		return bResult;
	}

	HANDLE hSelf = OpenProcess(PROCESS_ALL_ACCESS, FALSE, GetCurrentProcessId());

	MEMORY_BASIC_INFORMATION mbi = { 0 };
	DWORD dwSize = sizeof(MEMORY_BASIC_INFORMATION);


	if (VirtualQueryEx(hSelf, (LPVOID)(*((PADDRRESS)pInterface)), &mbi, dwSize) != dwSize )
	{
		return bResult;
	}

	LPBYTE lpBase = (LPBYTE)mbi.BaseAddress; 	
	DWORD dwOldProtect = 0;
	if (!VirtualProtectEx(hSelf, (LPVOID)lpBase, sizeof(PROC) * nFunIndex, PAGE_EXECUTE_READWRITE, &dwOldProtect))
	{
		DbgViewMessage(_T("[ModifyVirtualFunTable] VirtualProtectEx 1 Failed!\n"));
		return bResult;
	}


	PADDRRESS *pFunBase=(PADDRRESS*)pInterface;
	{
		SIZE_T nSize = sizeof(PROC);
		SIZE_T nWrittenSize = 0;
		LPVOID lpDesAddr = (LPVOID)(*pFunBase+nFunIndex-1);


		WriteProcessMemory(hSelf, (LPVOID)pSaveOldFun, lpDesAddr, nSize, &nWrittenSize);
		if( nWrittenSize != nSize )
		{
			DbgViewMessage(_T("[ModifyVirtualFunTable] WriteProcessMemory 1 Failed!\n"));
			return bResult;
		}

		WriteProcessMemory(hSelf, lpDesAddr, (LPVOID)&pNewFun, nSize, &nWrittenSize);
		if( nWrittenSize != nSize )
		{
			DbgViewMessage(_T("[ModifyVirtualFunTable] WriteProcessMemory 2 Failed!\n"));
			return bResult;
		}
	}

	DWORD dwFake=0;
	if (!VirtualProtectEx(hSelf,(LPVOID)lpBase,sizeof(PROC)*nFunIndex,dwOldProtect,&dwFake))
	{
		DbgViewMessage(_T("[ModifyVirtualFunTable] VirtualProtectEx 2 Failed!\n"));
		return bResult;
	}

	DbgViewMessage(_T("[ModifyVirtualFunTable] Success!\n"));

	return TRUE;
}