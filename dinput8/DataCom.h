#pragma once

#define		STR_NAME_SHARE_MEM		_T("Global\\Dinput8Ctrl")

typedef struct _MOUSE_DATA 
{
	LIST_ENTRY			ListEntry;
	DIDEVICEOBJECTDATA	Data;

}MOUSE_DATA, *PMOUSE_DATA;
#define	SizeOfMouseData		sizeof(MOUSE_DATA)

typedef struct _KEYBOARD_DATA 
{
	LIST_ENTRY			ListEntry;
	USHORT				DIK_Key;
	USHORT				KeyData;

}KEYBOARD_DATA, *PKEYBOARD_DATA;
#define SizeOfKeyboardData	sizeof(KEYBOARD_DATA)

void InitData();
void UnInitData();

void Lock(LPCRITICAL_SECTION lpLock);
void UnLock(LPCRITICAL_SECTION lpLock);

void AddOneMouse(LPDIDEVICEOBJECTDATA lpData);
BOOL GetOneMouse(LPDIDEVICEOBJECTDATA lpData);

void AddOneKeyboard(USHORT DIK_Key, USHORT KeyData);
BOOL GetOneKeyboard(char *lpKeyboard, ULONG dwSize);