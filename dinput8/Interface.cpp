#include "stdafx.h"
#include <dinput.h>

#include "Interface.h"
#include "DataCom.h"
#include "Log.h"

//
// 说明：鼠标点击，不通过Dinput8中的，鼠标移动的时候的接口。
//

//
// 构造鼠标移动数据
//
void FormatMouseMove(PPOINT Point, LPDIDEVICEOBJECTDATA rgdod, BOOL bX)
{
	// Point相对偏移
	rgdod[0].dwOfs = bX ? DIMOFS_X : DIMOFS_Y;
	rgdod[0].dwData = bX ? Point->x : Point->y;

	rgdod[0].uAppData = 0xFFFFFFFF;
}

BOOL MouseMoveTo(IN PPOINT Point, IN BOOL bReleative)
{
	if ( Point == NULL )
	{
		DbgViewMessage(_T("[MouseMoveTo] Parameter Error! Point: %#x\n"), Point);
		return FALSE;
	}

	DIDEVICEOBJECTDATA	Data = { 0 };

	if ( Point->x )
	{
		// 添加X轴的移动
		FormatMouseMove(Point, &Data, TRUE);
		AddOneMouse(&Data);
	}

	if ( Point->y )
	{
		// 添加Y轴的移动
		FormatMouseMove(Point, &Data, FALSE);
		AddOneMouse(&Data);
	}

	return TRUE;
}