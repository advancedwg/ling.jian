#include "stdafx.h"
#include <winnt.h>
#include <dinput.h>
#include <malloc.h>

#include "Win32ListEntry.h"
#include "DataCom.h"
#include "Log.h"

LIST_ENTRY			g_MouseHead;
CRITICAL_SECTION	g_MouseLock; 

LIST_ENTRY			g_KeyboardHead;
CRITICAL_SECTION	g_KeyboardLock;

void InitData()
{
	InitializeListHead(&g_MouseHead);
	InitializeListHead(&g_KeyboardHead);

	InitializeCriticalSection(&g_MouseLock);
	InitializeCriticalSection(&g_KeyboardLock);
}

void UnInitData()
{
	DeleteCriticalSection(&g_MouseLock);
	DeleteCriticalSection(&g_KeyboardLock);
}

void Lock(LPCRITICAL_SECTION lpLock)
{
	EnterCriticalSection(lpLock);
}

void UnLock(LPCRITICAL_SECTION lpLock)
{
	LeaveCriticalSection(lpLock);
}

void AddOneMouse(LPDIDEVICEOBJECTDATA lpData)
{
	if ( lpData == NULL )
	{
		DbgViewMessage(_T("[AddOneMouse] Parameter Error! lpData: %#x\n"), lpData);
		return ;
	}

	PMOUSE_DATA	pNewOne = (PMOUSE_DATA)malloc(SizeOfMouseData);
	if ( pNewOne )
	{
		ZeroMemory(pNewOne, SizeOfMouseData);
		CopyMemory(&(pNewOne->Data), lpData, sizeof(DIDEVICEOBJECTDATA));

		Lock(&g_MouseLock);
		InsertTailList(&g_MouseHead, &(pNewOne->ListEntry));
		UnLock(&g_MouseLock);
	}
}

BOOL GetOneMouse(LPDIDEVICEOBJECTDATA lpData)
{
	PLIST_ENTRY	pListEntry = NULL;

	if ( lpData == NULL )
	{
		DbgViewMessage(_T("[GetOneMouse] Parameter Error!\n"));
		return FALSE;
	}

	if ( !IsListEmpty(&g_MouseHead) )
	{
		Lock(&g_MouseLock);
		pListEntry = RemoveHeadList(&g_MouseHead);
		UnLock(&g_MouseLock);

		PMOUSE_DATA lpOne = CONTAINING_RECORD(pListEntry, MOUSE_DATA, ListEntry);
		CopyMemory(lpData, &(lpOne->Data), sizeof(DIDEVICEOBJECTDATA));

		free(lpOne); lpOne = NULL;

		return TRUE;
	}

	return FALSE;
}

void AddOneKeyboard(USHORT DIK_Key, USHORT KeyData)
{
	PKEYBOARD_DATA pNewOne = (PKEYBOARD_DATA)malloc(SizeOfKeyboardData);
	if ( pNewOne )
	{
		ZeroMemory(pNewOne, SizeOfKeyboardData);
		pNewOne->DIK_Key = DIK_Key;
		pNewOne->KeyData = KeyData;

		Lock(&g_KeyboardLock);
		InsertTailList(&g_KeyboardHead, &(pNewOne->ListEntry));
		UnLock(&g_KeyboardLock);
	}
}
BOOL GetOneKeyboard(char *lpKeyboard, ULONG dwSize)
{
	PLIST_ENTRY	pListEntry = NULL;

	if ( lpKeyboard == NULL || dwSize != 256 )
	{
		DbgViewMessage(_T("[GetOneKeyboard] Parameters Error! lpKeyboard: %#x, dwSize: %#x(%u(\n"), 
			lpKeyboard, dwSize, dwSize);
		return FALSE;
	}

	if ( !IsListEmpty(&g_KeyboardHead) )
	{
		Lock(&g_KeyboardLock);
		pListEntry = RemoveHeadList(&g_KeyboardHead);
		UnLock(&g_KeyboardLock);

		PKEYBOARD_DATA lpOne = CONTAINING_RECORD(pListEntry, KEYBOARD_DATA, ListEntry);
		lpKeyboard[lpOne->DIK_Key] = (char)lpOne->KeyData;

		free(lpKeyboard); lpKeyboard = NULL;

		return TRUE;
	}

	return FALSE;
}