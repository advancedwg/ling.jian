#pragma once

#define  PADDRRESS LPDWORD

#define	HOOK_INDEX_CreateDevice			0x04
#define	HOOK_INDEX_Acquire				0x08
#define	HOOK_INDEX_GetDeviceState		0x0A
#define HOOK_INDEX_GetDeviceData		0x0B
#define HOOK_INDEX_SetDataFormat		0x0C
#define	HOOK_INDEX_SetCooperativeLevel	0x0E
#define	HOOK_INDEX_Poll					0x1A


//------------------------------------------
//------------------------------------------
BOOL HookCreateDevice(LPVOID pInterface);
BOOL HookInterface(LPVOID pDevice);
BOOL HookInterfaceByDevice(LPVOID pDevice, ULONG IndexFun, const PROC NewFun);

//------------------------------------------
// Hook如下函数：
// 1. Acquire
// 2. Poll
// 3. GetDeviceState
//------------------------------------------
// BOOL HookAcquire(LPVOID pDevice, TYPE_DEV Type);
// BOOL HookPoll(LPVOID pDevice, TYPE_DEV Type);
// BOOL HookGetDeviceState(LPVOID pDevice, TYPE_DEV Type);

//------------------------------------------
// Com Hook功能函数
//------------------------------------------
BOOL ModifyVirtualFunTable(LPVOID pInterface, int nFunIndex, const PROC pNewFun, PROC *pSaveOldFun);

//------------------------------------------
// 注意第一个参数
//------------------------------------------
typedef
HRESULT 
(STDMETHODCALLTYPE *pfnCreateDevice)(
	LPDIRECTINPUT8, 
	REFGUID, 
	LPDIRECTINPUTDEVICE8 *, 
	LPUNKNOWN
	);

// typedef 
// HRESULT
// (STDMETHODCALLTYPE *pfnAcquire)(
// 	LPDIRECTINPUTDEVICE8
// 	);
// 
// typedef 
// 	HRESULT
// 	(STDMETHODCALLTYPE *pfnPoll)(
// 	LPDIRECTINPUTDEVICE8
// 	);

typedef
	HRESULT 
	(STDMETHODCALLTYPE *pfnGetDeviceState)(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	ULONG	dwBufSize, 
	LPVOID	lpBuf
	);

typedef
	HRESULT 
	(STDMETHODCALLTYPE *pfnSetCooperativeLevel)(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	HWND hwnd, 
	DWORD dwFlags
	);

typedef
	HRESULT 
	(STDMETHODCALLTYPE *pfnSetDataFormat)(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	LPCDIDATAFORMAT lpdf
	);

typedef
	HRESULT 
	(STDMETHODCALLTYPE *pfnGetDeviceData)(
	LPDIRECTINPUTDEVICE8 lpDevice, 
	DWORD cbObjectData,
	LPDIDEVICEOBJECTDATA rgdod,
	LPDWORD pdwInOut,
	DWORD dwFlags
	);