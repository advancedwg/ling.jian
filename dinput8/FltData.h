#pragma once

void InitFltProc();
BOOL AddOneProc(LPCTSTR lpszProcName);
BOOL DelOneProc(LPCTSTR lpszProcName);
BOOL FindOneProc(LPCTSTR lpszProcName);

BOOL IsHandleProcess();

void InitMemShared();
void UnInitMemShared();
BOOL IsStartDinput8();