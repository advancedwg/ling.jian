#pragma once

typedef
HRESULT  
(WINAPI *pfnDirectInput8Create)(
	HINSTANCE hinst, 
	DWORD dwVersion, 
	REFIID riidltf, 
	LPVOID *ppvOut, 
	LPUNKNOWN punkOuter
	);


typedef
HRESULT
(STDAPICALLTYPE *pfnDllCanUnloadNow)(
	void
	);

typedef
HRESULT
(STDAPICALLTYPE *pfnDllGetClassObject)(
	const CLSID & rclsid,  
	const IID & riid,      
	void ** ppv
	);

typedef pfnDllCanUnloadNow pfnDllRegisterServer;
typedef pfnDllCanUnloadNow pfnDllUnregisterServer;


//------------------------------------------
// Function
//------------------------------------------
typedef 
	BOOL 
	(WINAPI *ptr_IsWow64Process)(
	__in HANDLE hProcess, 
	__out PBOOL wow64Process
	);

BOOL IsX64Os();
BOOL LoadDinput8Dll();