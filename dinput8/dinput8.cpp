#include "stdafx.h"
#include <dinput.h>

#include "dinput8.h"
#include "InputHook.h"
#include "Log.h"
#include "FltData.h"
#include "DataCom.h"

#pragma comment(lib, "dxguid.lib")

//------------------------------------------
// Global variables
// g_hModuleDinput8: handle for dll.
//------------------------------------------
HMODULE	g_hModuleDinput8 = NULL;
PVOID	g_pfnDinput8Create = NULL;
PVOID	g_pfnDllCanUnloadNow = NULL;
PVOID	g_pfnDllGetClassObject = NULL;
PVOID	g_pfnRegisterServer = NULL;
PVOID	g_pfnUnregisterServer = NULL;

//------------------------------------------
// 1. Point to the directinput interface;
//	use it to hook CreateDevice;
//------------------------------------------
LPDIRECTINPUT8	g_lpDirectInputInterface = NULL;

//------------------------------------------
// Export by this DLL.
//------------------------------------------
HRESULT WINAPI
DirectInput8Create(
	HINSTANCE hinst, 
	DWORD dwVersion, 
	REFIID riidltf, 
	LPVOID *ppvOut, 
	LPUNKNOWN punkOuter
	)
{
	BOOL	brt = FALSE;

	DbgViewMessage(_T("[DirectInput8Create] Start!\n"));

	// 初始化过滤数据
	InitFltProc();

	InitMemShared();

	InitData();

	// 是否已Load
	if ( IS_NULL(g_pfnDinput8Create) )
	{
		brt = LoadDinput8Dll();
		if ( !brt )
		{
			DbgViewMessage(_T("[DirectInput8Create] LoadDinput8Dll Failed!\n"));
			return NULL;
		}
	}

	// 调用原始的函数
	DbgViewMessage(_T("[DirectInput8Create] Call old DirectInput8Create!\n"));
	HRESULT hResult  = ((pfnDirectInput8Create)g_pfnDinput8Create)(hinst, dwVersion, riidltf, ppvOut, punkOuter);
	if ( SUCCEEDED(hResult) && riidltf == IID_IDirectInput8 && IsHandleProcess() )
	{
		g_lpDirectInputInterface = (LPDIRECTINPUT8)(*ppvOut);
		DbgViewMessage(_T("[DirectInput8Create] g_lpDirectInputInterface: %#x\n"), g_lpDirectInputInterface);

		// Hook CreateDevice;
		HookCreateDevice(g_lpDirectInputInterface);
	}

	return hResult;
}

STDAPI
DllCanUnloadNow(
	void
	)
{
	DbgViewMessage(_T("DllCanUnloadNow\n"));
	return ((pfnDllCanUnloadNow)g_pfnDllCanUnloadNow)();
}

STDAPI
DllGetClassObject(
	const CLSID & rclsid,  
	const IID & riid,      
	void ** ppv
	)
{
	DbgViewMessage(_T("DllGetClassObject\n"));
	return ((pfnDllGetClassObject)g_pfnDllGetClassObject)(rclsid, riid, ppv);
}

STDAPI
DllRegisterServer(
	void
	)
{
	DbgViewMessage(_T("DllRegisterServer\n"));
	return ((pfnDllRegisterServer)g_pfnRegisterServer)();
}

STDAPI
DllUnregisterServer(
	void
	)
{
	DbgViewMessage(_T("DllUnregisterServer\n"));
	return ((pfnDllUnregisterServer)g_pfnUnregisterServer)();
}

BOOL LoadDinput8Dll()
{
	TCHAR	szDinput8[MAX_PATH] = { 0 };

	// 1. 获取Windows目录
	GetWindowsDirectory(szDinput8, MAX_PATH);

	// 2. 判断系统是否是x64。决定原始dinput8.dll的路径
	if ( IsX64Os() )
	{
		//  本DLL为32位，在x64系统上，用32位的Dinput8.dll
		DbgViewMessage(_T("[LoadDinput8Dll] This is x64 OS!\n"));
		_tcscat_s(szDinput8, _T("\\System32\\dinput8.dll"));
	}
	else
	{
		// 在x86系统上
		DbgViewMessage(_T("[LoadDinput8Dll] This is x86 OS!\n"));
		_tcscat_s(szDinput8, _T("\\System32\\dinput8"));
	}

	// 3. LoadLibrary原始的dinput8.dll
	DbgViewMessage(_T("[LoadDinput8Dll] %ws"), szDinput8);
	g_hModuleDinput8 = LoadLibrary(szDinput8);
	if ( g_hModuleDinput8 )
	{
		DbgViewMessage(_T("[LoadDinput8Dll] LoadLibrary Success!\n"));

		// 4. 保存原始的dinput8.dll中的函数地址，通过调用GetProcAddress
		g_pfnDinput8Create = GetProcAddress(g_hModuleDinput8, "DirectInput8Create");
		g_pfnDllCanUnloadNow = GetProcAddress(g_hModuleDinput8, "DllCanUnloadNow");
		g_pfnDllGetClassObject = GetProcAddress(g_hModuleDinput8, "DllGetClassObject");
		g_pfnRegisterServer = GetProcAddress(g_hModuleDinput8, "DllRegisterServer");
		g_pfnUnregisterServer = GetProcAddress(g_hModuleDinput8, "DllUnregisterServer");
	}
	else
	{
		DbgViewMessage(_T("[LoadDinput8Dll] LoadLibrary Failed! Error: %u\n"), GetLastError());

		SetLastError(GetLastError());

		return FALSE;
	}

	// 4. 如果各函数获取失败，DLL加载失败
	if ( IS_NULL(g_pfnDinput8Create) || 
		IS_NULL(g_pfnDllCanUnloadNow) || 
		IS_NULL(g_pfnDllGetClassObject) || 
		IS_NULL(g_pfnRegisterServer) || 
		IS_NULL(g_pfnUnregisterServer) 
		)
	{
		DbgViewMessage(_T("[LoadDinput8Dll] Obtain the Original Dll functions Failed!\n"));
		return FALSE;
	}

	return TRUE;
}

BOOL IsX64Os()
{
	BOOL ret = TRUE;
	HMODULE kernel32 = GetModuleHandle(_T("kernel32"));

	//
	// Get the address of the functions in wow64.
	//
	ptr_IsWow64Process pfnIsWow64Process = (ptr_IsWow64Process)GetProcAddress(kernel32, "IsWow64Process");
	if ( pfnIsWow64Process == NULL )
	{
		DbgViewMessage(_T("[IsX64Os] GetProcAddress IsWow64Process Failed!\n"));
		return FALSE;
	}

	//
	// Check the process whether it is running in wow64.
	//
	BOOL isWow = FALSE;
	if (!pfnIsWow64Process(GetCurrentProcess(), &isWow))
	{
		DbgViewMessage(_T("[IsX64Os] IsWow64Process Failed!\n"));
		isWow = FALSE;

		return FALSE;
	}

	//
	// Close the wow64, when it is under wow64.
	//
	return isWow;
}